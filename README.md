# Lint Rules

## Description

This package contains a standard set of ESLint rules to be used across Microexcel CSD projects. This is to ensure each project has the same set of rules to keep projects consistent.

## Installation

From the root folder of your project, install the package with the following command:
```javascript
npm i @microexcel-csd/lint-rules --save-dev
```

## Usage

Once the package is installed, add the desired rulesets to the `"extends"` section of the `eslintrc.json` in the root of your project:
```javascript
{
    "root": true,
    "ignorePatterns": [
        "projects/**/*"
    ],
    "overrides": [
        {
            "files": [
                "*.ts"
            ],
            "parserOptions": {
                "project": [
                    "tsconfig.json"
                ],
                "createDefaultProgram": true
            },
            "extends": [
                "@microexcel-csd/lint-rules/angular"
            ],
            ...
        }
        ...
    ]
}
```

Afterwards, you will be able to run `ng lint` to use the newly-imported lint rules.

> **NOTE: Keeping this package up to date is highly recommended. Be sure that the version number in your `package.json` contains a `^` before the version number to update to the newest minor version when using `npm update`.**

**Example:**
```javascript
"devDependencies": {
    ...
    "@microexcel-csd/lint-rules": "^2.0.0",
    ...
}
```
